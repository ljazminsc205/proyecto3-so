import com.sun.tools.javac.Main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import Main.MiFileSystem;
import Main.Carpeta;
import Main.Archivo;

public class GUI {
    public JPanel panel1;
    private JTextArea rootTextArea;
    private JButton addFileButton;
    private JButton addFolderButton;
    private JButton homeButton;
    private JPanel espacioMuestra;
    private JButton btnAtras;
    private JButton verDiscoButton;
    private JLabel estadoDisco;
    private JPanel PanelThree;
    private MiFileSystem miFileSystem;
    static JFrame jFrame;
    JTextField nombreCarpeta;
    JTextField ruta;
    String nombrePegar;
    String contPegar;
    JButton creaCarpeta;
    JButton copiarAPC;
    JButton buscar;
    JTextField nombreArchivo;
    JButton creaArchivo;
    JTextArea contArchivo;
    JLabel estado;
    JList archivos;
    JPopupMenu submenu;
    JLabel three;

    public GUI() {
        miFileSystem = new MiFileSystem();

        three = new JLabel();
        PanelThree.setLayout(new FlowLayout());
        PanelThree.add(three);
        PanelThree.setVisible(true);

        estadoDisco.setText("Porcentaje libre: ");

        estado = new JLabel();
        archivos = new JList();
        espacioMuestra.setLayout(new GridLayout(3,3,10,10));
        espacioMuestra.add(archivos);
        espacioMuestra.setVisible(true);
        btnAtras.setText("atras");
        submenu = new JPopupMenu();

        JMenuItem info = new JMenuItem("info");
        // aqui se hace la logica de la opcion de info
        info.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int index = archivos.getSelectedIndex();
                String archivo = (String) archivos.getModel().getElementAt(index);
                JDialog d = new JDialog(jFrame,"Info "+archivo);
                JLabel l = new JLabel(miFileSystem.verInfoArchivo(archivo));
                d.setLayout(new FlowLayout());
                d.add(l);
                d.setLocationRelativeTo(jFrame);
                d.setSize(500, 200);
                d.setVisible(true);
            }
        });

        JMenuItem copy1 = new JMenuItem("copiar Real a Virtual");
        copy1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                JDialog d = new JDialog(jFrame,"Copiar Archivo del Computador");
                JLabel l = new JLabel("Ingrese la ruta donde se encuentre el archivo");
                JLabel l1 = new JLabel("Ingrese el nombre del archivo");
                ruta = new JTextField();
                nombreArchivo = new JTextField();
                copiarAPC = new JButton("Copiar");
                d.setLayout(new GridLayout(3,0,10,10));
                d.add(l);
                d.add(ruta);
                d.add(l1);
                d.add(nombreArchivo);
                d.add(copiarAPC);
                d.setLocationRelativeTo(jFrame);
                d.setSize(500, 200);
                d.setVisible(true);
                copiarAPC.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Archivo temp = miFileSystem.getDirectorioActual().getArchivo(nombreArchivo.getText());
                        if (temp==null) {
                            String contenido=miFileSystem.copy(1,nombreArchivo.getText(),ruta.getText());
                            miFileSystem.crearArchivo(nombreArchivo.getText(),contenido);
                        } else {
                            String contenido=miFileSystem.copy(1,nombreArchivo.getText(),ruta.getText());
                            confirmaSobreescritura(nombreArchivo.getText(),contenido);
                        }

                        updateEstado();
                        d.dispose();
                    }
                });

            }
        });

        JMenuItem copy2 = new JMenuItem("copiar Virtual a Real");
        copy2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int index = archivos.getSelectedIndex();
                String archivo = (String) archivos.getModel().getElementAt(index);
                JDialog d = new JDialog(jFrame,"Copiar "+ archivo);
                JLabel l = new JLabel("Ingrese la ruta de su computadora donde desee copiarlo");
                ruta = new JTextField();
                copiarAPC = new JButton("Copiar");
                d.setLayout(new GridLayout(3,0,10,10));
                d.add(l);
                d.add(ruta);
                d.add(copiarAPC);
                d.setLocationRelativeTo(jFrame);
                d.setSize(500, 200);
                d.setVisible(true);
                copiarAPC.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        // hacer verificacion de que no exista uno con el mismo nombre
                        miFileSystem.copy(2,archivo,ruta.getText());
                        updateEstado();
                        d.dispose();
                    }
                });
            }
        });
        JMenuItem copy3 = new JMenuItem("copiar");
        copy3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int index = archivos.getSelectedIndex();
                nombrePegar = (String) archivos.getModel().getElementAt(index);
                contPegar= miFileSystem.verContArchivo(nombrePegar);

            }
        });

        JMenuItem paste = new JMenuItem("pegar");
        paste.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Archivo temp = miFileSystem.getDirectorioActual().getArchivo(nombrePegar);
                if (temp==null) {
                    miFileSystem.crearArchivo(nombrePegar,contPegar);
                } else {
                    confirmaSobreescritura(nombrePegar,contPegar);
                }

                updateEstado();

            }
        });

        JMenuItem move = new JMenuItem("mover");

        move.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int index = archivos.getSelectedIndex();
                String archivo = (String) archivos.getModel().getElementAt(index);
                JDialog d = new JDialog(jFrame, "Mover");
                JLabel labelOrigen = new JLabel("Origen:");
                JTextField origen = new JTextField(miFileSystem.getRutaActual()+archivo);
                JLabel labelDestino = new JLabel("Destino:");
                JTextField destino = new JTextField();
                JButton mover = new JButton("mover");
                d.setLayout(new GridLayout(5,0,10,10));
                d.add(labelOrigen);
                d.add(origen);
                d.add(labelDestino);
                d.add(destino);
                d.add(mover);
                d.setSize(500,200);
                d.setLocationRelativeTo(jFrame);
                d.setVisible(true);
                mover.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        miFileSystem.moverArchivo(origen.getText(), destino.getText());
                        updateEstado();
                        d.dispose();
                    }
                });
            }
        });
        // para eliminar el archivo
        JMenuItem delete = new JMenuItem("eliminar");

        delete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int index = archivos.getSelectedIndex();
                String archivo = (String) archivos.getModel().getElementAt(index);
                String[] temp = archivo.split("/");
                if (temp.length>1) {
                    miFileSystem.llamarVersion2(temp[0]);
                } else {
                    miFileSystem.eliminarArchivo(temp[0]);
                }
                updateEstado();
            }
        });

        JMenuItem find = new JMenuItem("buscar");
        find.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                JDialog d = new JDialog(jFrame,"Buscar archivo");
                JLabel l = new JLabel("Ingrese el nombre del archivo");

                nombreArchivo = new JTextField();
                buscar= new JButton("Buscar");
                d.setLayout(new GridLayout(3,0,10,10));
                d.add(l);

                d.add(nombreArchivo);
                d.add(buscar);
                d.setLocationRelativeTo(jFrame);
                d.setSize(500, 200);
                d.setVisible(true);
                buscar.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        // hacer verificacion de que no exista uno con el mismo nombre
                        JDialog d1 = new JDialog(jFrame,"Rutas donde se encuentra  "+nombreArchivo.getText());
                        d1.setLayout(new FlowLayout());

                        JLabel l1 = new JLabel(miFileSystem.getRutas(nombreArchivo.getText()));

                        d1.add(l1);
                        d1.setLocationRelativeTo(jFrame);
                        d1.setSize(500, 200);
                        d1.setVisible(true);

                    }
                });


            }
        });


        submenu.add(info);
        submenu.add(copy1);
        submenu.add(copy2);
        submenu.add(copy3);
        submenu.add(paste);
        submenu.add(move);
        submenu.add(delete);
        submenu.add(find);

        // boton para ir al root
        homeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                miFileSystem.irARoot();
                rootTextArea.setText(miFileSystem.getRutaActual());
                updateEstado();
            }
        });

        //boton para agregar una carpeta
        addFolderButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JDialog d = new JDialog(jFrame,"Crear carpeta");
                JLabel l = new JLabel("Ingrese el nombre de la carpeta:");
                nombreCarpeta = new JTextField();
                creaCarpeta = new JButton("Crear");
                d.setLayout(new GridLayout(3,0,10,10));
                d.add(l);
                d.add(nombreCarpeta);
                d.add(creaCarpeta);
                d.setLocationRelativeTo(jFrame);
                d.setSize(500, 200);
                d.setVisible(true);
                creaCarpeta.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Carpeta temp = miFileSystem.getDirectorioActual().getDirectorio(nombreCarpeta.getText());
                        if (temp == null) {
                            miFileSystem.crearCarpeta(nombreCarpeta.getText());
                        } else {
                            confirmaSobreescrituraDir(nombreCarpeta.getText());
                        }
                        updateEstado();
                        d.dispose();
                    }
                });

            }
        });

        // boton para agregar un archivo
        addFileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JDialog d = new JDialog(jFrame,"Crear archivo");
                JLabel l = new JLabel("Ingrese el nombre del archivo:");
                JLabel l1 = new JLabel("Ingrese el contenido del archivo:");
                nombreArchivo = new JTextField();
                contArchivo = new JTextArea();
                creaArchivo = new JButton("Crear");
                d.setLayout(new GridLayout(5,0,10,10));
                d.add(l);
                d.add(nombreArchivo);
                d.add(l1);
                d.add(contArchivo);
                d.add(creaArchivo);
                d.setLocationRelativeTo(jFrame);
                d.setSize(500, 200);
                d.setVisible(true);
                creaArchivo.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Archivo temp = miFileSystem.getDirectorioActual().getArchivo(nombreArchivo.getText());
                        if (temp==null) {
                            miFileSystem.crearArchivo(nombreArchivo.getText(), contArchivo.getText());
                        } else {
                            confirmaSobreescritura(nombreArchivo.getText(), contArchivo.getText());
                        }
                        updateEstado();
                        d.dispose();
                    }
                });
            }
        });

        // listener de la lista que tiene muestra los archivos y carpetas
        archivos.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int index = archivos.getSelectedIndex();
                    String archivo = (String) archivos.getModel().getElementAt(index);
                    String[] temp = archivo.split("/");
                    if (temp.length>1) {
                        miFileSystem.cambiarDirectorio(temp[0]);
                        updateRuta();
                        updateEstado();
                    } else {
                        muestraArchivo(temp[0]);
                    }
                } else if (e.isPopupTrigger()) {
                    dopop(e);
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    dopop(e);
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    dopop(e);
                }
            }

            private void dopop(MouseEvent e) {
                submenu.show(archivos, e.getX(), e.getY());
            }
        });

        // boton directorio anterior
        btnAtras.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!miFileSystem.getDirectorioActual().getNombre().equals("root")) {
                    miFileSystem.irAAnterior();
                    updateRuta();
                    updateEstado();
                }
            }
        });
        verDiscoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JDialog d = new JDialog(jFrame, "Disco virtual");
                JLabel l = new JLabel(miFileSystem.verDisco());
                d.setLayout(new FlowLayout());
                d.add(l);
                d.setSize(500,200);
                d.setVisible(true);
            }
        });
    }

    private void confirmaSobreescritura(String nombre, String cont) {
        JDialog d = new JDialog(jFrame, "Confirmar");
        JLabel l = new JLabel("Ya existe un archivo con este nombre. ¿Quiere caerle encima?");
        JButton aceptar = new JButton("aceptar");
        d.setLayout(new GridLayout(2,0,10,10));
        d.add(l);
        d.add(aceptar);
        d.setSize(500,200);
        d.setLocationRelativeTo(jFrame);
        d.setVisible(true);
        aceptar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                miFileSystem.actualizarArchivo(nombre, cont);
                d.dispose();
            }
        });
    }

    private void confirmaSobreescrituraDir(String nombre) {
        JDialog d = new JDialog(jFrame, "Confirmar");
        JLabel l = new JLabel("Ya existe una carpeta con este nombre. ¿Quiere caerle encima?");
        JButton aceptar = new JButton("aceptar");
        d.setLayout(new GridLayout(2,0,10,10));
        d.add(l);
        d.add(aceptar);
        d.setSize(500,200);
        d.setLocationRelativeTo(jFrame);
        d.setVisible(true);
        aceptar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                miFileSystem.llamarVersion2(nombre);
                miFileSystem.crearCarpeta(nombre);
                d.dispose();
            }
        });
    }

    // abre una ventana con el archivo para editarlo
    private void muestraArchivo(String nombre) {
        JDialog d = new JDialog(jFrame,"Editar archivo");
        JLabel l = new JLabel("Ingrese el nombre del archivo:");
        JLabel l1 = new JLabel("Ingrese el contenido del archivo:");
        nombreArchivo = new JTextField(nombre);
        nombreArchivo.setEnabled(false);
        contArchivo = new JTextArea(miFileSystem.verContArchivo(nombre));
        JButton guardaArchivo = new JButton("Guardar");
        d.setLayout(new GridLayout(5,0,10,10));
        d.add(l);
        d.add(nombreArchivo);
        d.add(l1);
        d.add(contArchivo);
        d.add(guardaArchivo);
        d.setLocationRelativeTo(jFrame);
        d.setSize(500, 200);
        d.setVisible(true);
        guardaArchivo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                miFileSystem.actualizarArchivo(nombre, contArchivo.getText());
                d.dispose();
            }
        });
    }

    // actualiza la lista que muestra los archivos y carpetas
    private void updateEstado(){
        archivos.removeAll();
        Carpeta dirActual = miFileSystem.getDirectorioActual();
        List<Carpeta> listaCar = dirActual.getCarpetas();
        DefaultListModel<String> listTemp = new DefaultListModel<String>();
        for (Carpeta carpeta:listaCar){
            String elem = carpeta.getNombre()+"/ ";
            listTemp.addElement(elem);
        }

        List<Archivo> listArchivos = dirActual.getArchivos();
        for (Archivo arch:listArchivos) {
            String elem = arch.getNombre();
            listTemp.addElement(elem);
        }
        archivos.setModel(listTemp);
        String discoLibre = miFileSystem.getEspacioLibre();
        estadoDisco.setText("Porcentaje libre: "+discoLibre);
        three.setText(miFileSystem.getThree());
    }

    // actualiza la ruta
    private void updateRuta() {
        rootTextArea.setText(miFileSystem.getRutaActual());
    }

    // inicializa el disco virtual
    public void iniciaDisco() {
        JDialog d = new JDialog(jFrame,"Crear disco");
        JLabel l = new JLabel("Ingrese el tamano de los sectores:");
        JLabel l1 = new JLabel("Ingrese la cantidad de sectores:");
        JTextField tam = new JTextField();
        JTextField cant = new JTextField();
        JButton crearDisco = new JButton("crear");
        d.setLayout(new GridLayout(5,0,10,10));
        d.add(l);
        d.add(tam);
        d.add(l1);
        d.add(cant);
        d.add(crearDisco);
        d.setLocationRelativeTo(jFrame);
        d.setSize(500, 200);
        d.setVisible(true);
        crearDisco.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int tamSect = Integer.parseInt(tam.getText());
                int cantSect = Integer.parseInt(cant.getText());
                miFileSystem.crearDisco(tamSect, cantSect);
                d.dispose();
            }
        });
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }

    public static void main(String[] args) {
        JFrame.setDefaultLookAndFeelDecorated(true);
        jFrame = new JFrame("File System");
        GUI gui = new GUI();
        jFrame.setContentPane(gui.panel1);
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.pack();
        int frameWidth = 200;
        int frameHeight = 100;
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        jFrame.setBounds((int) screenSize.getWidth() - frameWidth, 0, frameWidth, frameHeight);
        jFrame.setVisible(true);
        gui.iniciaDisco();
    }
}

package Main;

import javax.naming.InterruptedNamingException;
import java.util.ArrayList;
import java.util.List;

public class Carpeta {

    int idCarpeta;
    String nombre;
    List<Archivo> archivos;
    List<Carpeta> carpetas;

    public Carpeta(String nom, int id) {
        nombre = nom;
        idCarpeta = id;
        carpetas = new ArrayList<Carpeta>();
        archivos = new ArrayList<Archivo>();
    }

    public void crearCarpeta(String nom, int id){
        Carpeta carpeta = new Carpeta(nom, id);
        carpetas.add(carpeta);
    }

    public void crearArchivo(String nombre, int id, List<Integer> sectores, int tamano) {
        Archivo archivo = new Archivo(nombre, id, sectores, tamano);
        archivos.add(archivo);
    }

    public String verInfoArchivo(String nombre) {
        String result = "";
        for (Archivo archivo : archivos) {
            if (archivo.getNombre().equals(nombre)) {
                result = archivo.getInfo();
            }
        }
        return result;
    }

    public List<Integer> getSectoresArchivo(String nombre) {
        List<Integer> result = new ArrayList<Integer>();
        for (Archivo archivo : archivos) {
            if (archivo.getNombre().equals(nombre)) {
                result = archivo.getSectores();
            }
        }
        return result;
    }

    public String getEstado() {
        String result = "";
        result += "Carpetas:\n";
        for (Carpeta carpeta : carpetas) {
            result += carpeta.nombre+"\n";
        }
        result += "Archivos:\n";
        for (Archivo archivo: archivos) {
            result += archivo.nombre+"\n";
        }
        return result;
    }

    public void udpateFile(String nombre , List<Integer> secs , int tam){
        for (Archivo archivo : archivos) {
            String name = archivo.getNombre();
            if (name.equals(nombre)) {
                archivo.updateFile(nombre , secs , tam);
            }
        }
    };

    public Carpeta getDirectorio(String nombre) {
        Carpeta result = null;
        for (Carpeta carpeta : carpetas) {
            if (carpeta.getNombre().equals(nombre)) {
                result = carpeta;
            }
        }
        return result;
    }

    public Archivo getArchivo(String nombre) {
        Archivo result = null;
        String name  = null;
        for (Archivo archivo : archivos) {
            name = archivo.getNombre();
            if (name.equals(nombre)) {
                result = archivo;
            }
        }
        return result;
    }

    public String getNombre() {
        return nombre;
    }

    public Integer eliminarArchivo(String nombre) {
        String name;
        Integer id = 0;
        List<Archivo> listTemp = new ArrayList<Archivo>();
        for (Archivo archivo : archivos) {
            name = archivo.getNombre();
            if (name.equals(nombre)) {
                id =archivo.getID();
            }
            else {
            listTemp.add(archivo);
            }
        }
        archivos=listTemp;
        return id;
    };

    public Integer getID() {
        return idCarpeta;
    }

    public Integer eliminarCarpetaVacia(String nombre){
        String name;
        Integer id = 0;
        List<Carpeta> listTemp = new ArrayList<Carpeta>();
        for (Carpeta carpeta : carpetas) {
            name = carpeta.getNombre();
            if (name.equals(nombre)) {
                id =carpeta.getID();
            }
            else {
                listTemp.add(carpeta);
            }
        }
        carpetas=listTemp;
        return id;
    };
    //------------
    //Buscar elemento en la Carpeta actual
    public Boolean findCarpeta(String dato){
        Boolean result = false;

        for (Carpeta carpeta : carpetas) {
            if (dato==carpeta.getNombre()){
                result=true;
            }
        }
        for (Archivo archivo: archivos) {
            if (dato==archivo.getNombre()){
                result=true;
            }
        }
        return result;
    }
    //Obtiene el nombre de la carpeta según la posición donde se encuentre
    public String carpetaActual(int num){
        int cont=1;
        String result="";
        for (Carpeta carpeta : carpetas) {
            if (cont==num) {
                result = carpeta.getNombre();
            }
            cont=cont+1;
        }
        return result;
    }
    //Obtiene cantidad de elementos de la ruta actual
    public int tamano(){
        int result=0;
        for (Carpeta carpeta : carpetas) {
            result +=1;
        }
        return result;
    }

    //-----------------

    public Integer getSizeCarpetas() {
        return carpetas.size();
    }

    public Integer getSizeArchivos() {
        return archivos.size();
    }

    public List <Archivo> getArchivos(){
        return archivos;
    };
    public List <Carpeta> getCarpetas(){
        return carpetas;
    };

    public void renameFile(String nombreActual , String nombreFuturo){
        String name;
        for (Archivo file: archivos){
            name=file.getNombre();
            if (name.equals(nombreActual)){
                file.updateName(nombreFuturo);
            }
        }
    };

    public void renameFolder(String nombreActual , String nombreFuturo){
        String nomb;
        for (Carpeta folder:carpetas){
            nomb = folder.nombre;
            if (nomb.equals(nombreActual)){
                folder.nombre = nombreFuturo;
            }
        }
    };

}

package Main;
import java.util.concurrent.*;

public class main {
    public static void main(String[] args) throws InterruptedException {
        MiFileSystem miFileSystem = new MiFileSystem();
        miFileSystem.crearDisco(5,10);
        miFileSystem.crearCarpeta("bin");
        miFileSystem.cambiarDirectorio("bin");
        miFileSystem.crearArchivo("hola.txt", "hola mundo");

        miFileSystem.crearCarpeta("prueba");
        miFileSystem.crearCarpeta("pruebaD");
        miFileSystem.crearCarpeta("prueba2");

        miFileSystem.cambiarDirectorio("prueba");

        miFileSystem.crearArchivo("hola.txt", "hola mundo");
        miFileSystem.crearCarpeta("prueba3");
        //System.out.println(miFileSystem.mostrarEstadoFileSystem());
        miFileSystem.irAAnterior();
        miFileSystem.cambiarDirectorio("prueba2");
        miFileSystem.crearCarpeta("prueba4");
        miFileSystem.crearArchivo("hola3.txt", "hola mundo");
        miFileSystem.crearCarpeta("prueba5");

        miFileSystem.cambiarDirectorio("prueba4");
        miFileSystem.crearArchivo("hola.txt", "aaa");
        miFileSystem.crearCarpeta("prueba41");
        miFileSystem.irAAnterior();

        miFileSystem.cambiarDirectorio("prueba5");
        miFileSystem.crearArchivo("hola5.doc", "h do");
        miFileSystem.crearArchivo("hola6.txt", "h o");
        miFileSystem.crearArchivo("hola7.txt", "o");

        miFileSystem.irARoot();
        miFileSystem.cambiarDirectorio("bin");
        miFileSystem.cambiarDirectorio("prueba2");
        //miFileSystem.moverArchivo("/bin/prueba2/hola3.txt" , "/bin/prueba2/hola5.txt");
        //miFileSystem.moverArchivo("/bin/prueba2/prueba4" , "/bin/prueba2/prueba7");

        //miFileSystem.crearArchivos("/bin/prueba2/prueba5" , "/bin/prueba");
        miFileSystem.moverArchivo("/bin/prueba2" , "/bin/prueba");
        miFileSystem.irRuta("/bin");



       System.out.println(miFileSystem.getRutas(".doc"));

        //System.out.println(miFileSystem.mostrarEstadoFileSystem());
        //miFileSystem.irRuta("/bin/prueba/prueba2");
        //System.out.println(miFileSystem.mostrarEstadoFileSystem());
        //miFileSystem.irRuta("/bin/prueba/prueba2/prueba4");
        //System.out.println(miFileSystem.mostrarEstadoFileSystem());
        //miFileSystem.irRuta("/bin/prueba/prueba2/prueba5");
        //System.out.println(miFileSystem.mostrarEstadoFileSystem());

        //miFileSystem.moverArchivo("/bin/prueba2/hola3.txt" , "/bin/prueba2/prueba5");

        //miFileSystem.irRuta("/bin/prueba2/prueba5");
        //System.out.println( miFileSystem.rutaDestinoEsArchivo("/bin/prueba2/hola3.txt"));
        //System.out.println( miFileSystem.rutaDestinoEsArchivo("/bin/prueba5"));

        //System.out.println(miFileSystem.mostrarEstadoFileSystem());
        //System.out.println(miFileSystem.verDisco());
        //miFileSystem.llamarVersion2("prueba5");
        //System.out.println(miFileSystem.mostrarEstadoFileSystem());

        //System.out.println(miFileSystem.verDisco());

        //miFileSystem.obtenerSizeCarpeta("prueba2");
        //miFileSystem.cambiarDirectorio("prueba5");

        //miFileSystem.crearArchivo("prueba.txt", "archivo prueba");
        //System.out.println(miFileSystem.mostrarEstadoFileSystem());
        //miFileSystem.eliminarArchivo("hola.txt");
        //miFileSystem.eliminarArchivo("prueba.txt");
        //miFileSystem.crearArchivo("prueba3.txt", "cc cccc");
        //System.out.println(miFileSystem.verDisco());
        //TimeUnit.MINUTES.sleep(1);
        //System.out.println(miFileSystem.verInfoArchivo("hola.txt"));
        //miFileSystem.actualizarArchivo("hola.txt", "este es el nuevo texto");
        //System.out.println(miFileSystem.verInfoArchivo("hola.txt"));
        //System.out.println(miFileSystem.verDisco());
        //miFileSystem.crearCarpeta("boot");
        //miFileSystem.crearArchivo("hola.txt", "este es el nuevo texto");
        //miFileSystem.verContArchivo("hola.txt");
        //System.out.println(miFileSystem.verContArchivo("hola.txt"));
        //System.out.println(miFileSystem.verInfoArchivo("ejecutable.sh"));
        //System.out.println(miFileSystem.mostrarEstadoFileSystem());
        //System.out.println(miFileSystem.mostrarEstadoFileSystem());
        //miFileSystem.irAAnterior();

        //System.out.println(miFileSystem.mostrarEstadoFileSystem());
        //miFileSystem.irARoot();
    }
}

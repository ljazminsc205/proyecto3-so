package Main;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DiscoV {

    private int tamanoSegmentos;
    private int cantSegmentos;
    private int cantSegLibres;
    private byte[][] sectores;
    private List<Integer> sectoresLibres;
    private List<Integer> ultInsercion;

    public void inicializar(int tamSeg, int cantSeg) {
        tamanoSegmentos = tamSeg;
        cantSegmentos = cantSeg;
        cantSegLibres = cantSeg;
        sectores = new byte[cantSeg][tamSeg];
        sectoresLibres = iniciarLibres(cantSeg);
    }

    private List<Integer> iniciarLibres(int cant){
        List<Integer> result = new ArrayList<Integer>();
        for (int i=0; i<cant; i++) {
            result.add(i);
        }
        return result;
    }

    public List<Integer> guardar(String contenido) {
        byte[] bytes = contenido.getBytes(StandardCharsets.UTF_8);
        if (hayEspacio(bytes.length)){
            return guardaContenido(bytes);
        } else {
            return new ArrayList<>();
        }
    }

    public String obtener(List<Integer> list) {
        String result = "";
        for (Integer integer : list) {
            String s = new String(sectores[integer]);
            result += s;
        }
        return result;
    }

    public void eliminar(List<Integer> list) {
        cantSegLibres += list.size();
        for (Integer i: list){
            sectoresLibres.add(i);
        }
    }

    public List<Integer> actualizar(List<Integer> list, String contenido) {
        byte[] bytes = contenido.getBytes(StandardCharsets.UTF_8);
        int cantSectores = calcSecRequeridos(bytes.length);
        if (hayEspacio(bytes.length) || cantSectores <= list.size()){
            eliminar(list);
            return guardar(contenido);
        } else {
            return new ArrayList<Integer>();
        }
    }

    private List<Integer> guardaContenido(byte[] bytes) {
        ultInsercion = new ArrayList<>();
        int asignados = 0;
        byte[] tempBytes = new byte[tamanoSegmentos];
        int tempTam = 0;

        for (int i=0; i<bytes.length; i++) {
            if (tempTam == tamanoSegmentos-1) {
                tempBytes[tempTam] = bytes[i];
                asignarBloque(tempBytes);
                asignados++;
                tempTam = 0;
                tempBytes = new byte[tamanoSegmentos];
            } else {
                tempBytes[tempTam] = bytes[i];
                tempTam++;
            }
        }

        if (tempTam>0) {
            asignarBloque(tempBytes);
            asignados++;
        }
        cantSegLibres -= asignados;
        List<Integer> indices = ultInsercion;
        System.out.println(indices.toString());
        return indices;
    }

    private boolean hayEspacio(int cantBytes) {
        int cantSectores = calcSecRequeridos(cantBytes);
        if (cantSectores <= cantSegLibres) {
            return true;
        } else {
            return false;
        }
    }

    private int calcSecRequeridos(int cantBytes) {
        int cantAsignar = cantBytes / tamanoSegmentos;
        int res = cantBytes % tamanoSegmentos;
        if (res!=0) {
            cantAsignar+=1;
        }
        return cantAsignar;
    }

    private void asignarBloque(byte[] bytes) {
        int ind = getBloque();
        for (int i=0; i<tamanoSegmentos; i++) {
            sectores[ind][i] = bytes[i];
        }
        ultInsercion.add(ind);
    }

    private int getBloque(){
        Random random = new Random();
        int bloque = sectoresLibres.get(random.nextInt(sectoresLibres.size()));
        sectoresLibres.remove(new Integer(bloque));
        return bloque;
    }

    public String mostrar(){
        String result = "<html>";
        for (int i=0; i<cantSegmentos; i++) {
            result += "seg"+i+"  ";
            for (int e=0; e<tamanoSegmentos; e++){
                result += sectores[i][e];
            }
            String s = new String(sectores[i]);
            result += " "+s+"\n<br>";
        }
        result+=sectoresLibres.toString()+"<br>";
        result+="sectores libres: "+cantSegLibres+"</html>";
        return result;
    }

    public String espacioLibre() {
        String espacio = "";
        double porc = 100.0 * cantSegLibres/cantSegmentos;
        espacio += String.format("%.1f",porc);
        return espacio;
    }

    public static void main(String[] args) {
        DiscoV disco = new DiscoV();
        disco.inicializar(8,10);
        List<Integer> list = disco.guardar("esto es un ejemplo, mas texto");
        System.out.println(list.toString());
        System.out.println(disco.obtener(list));
        //disco.eliminar(list);
        List<Integer> list1 = disco.actualizar(list, "hola mondo");
        //List<Integer> list1 = disco.guardar("hola mondo");
        if (!list1.isEmpty()) {
            System.out.println(list1.toString());
            System.out.println(disco.obtener(list1));
        }
        System.out.println(disco.mostrar());
    }

}

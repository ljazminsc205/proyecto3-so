package Main;

import java.util.Date;
import java.util.List;

public class Archivo {

    int idArchivo;
    String nombre;
    Date fechaCreacion;
    Date ultModificacion;
    List<Integer> sectores;
    int tamano;

    public Archivo(String nom, int id, List<Integer> secs, int tam) {
        nombre = nom;
        idArchivo = id;
        fechaCreacion = new Date();
        ultModificacion = fechaCreacion;
        sectores = secs;
        tamano = tam;
    }

    public Integer getID() {
        return idArchivo;
    }

    public String getNombre() {
        return nombre;
    }

    public List<Integer> getSectores() {
        return sectores;
    }

    public void updateFile(String nom, List<Integer> secs, int tam) {
        nombre = nom;
        ultModificacion = new Date();
        sectores = secs;
        tamano = tam;
    }

    public void updateName(String nombreFuturo){
        nombre=nombreFuturo;
    };

    public String getInfo() {
        String result = "<html>";
        result += "ID: " + idArchivo+ "\n<br>";
        result += "Nombre: "+ nombre+"\n<br>";
        result += "Fecha de creación: "+ fechaCreacion+"\n<br>";
        result += "Última modificación: "+ ultModificacion+"\n<br>";
        result += "Tamaño: "+tamano+" bytes \n<br></html>";
        return result;
    }

}

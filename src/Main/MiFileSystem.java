package Main;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.SimpleTimeZone;
import java.io.*;


public class MiFileSystem {

    DiscoV discoV;
    Carpeta root;
    int idCarpetas = 1;
    int idArchivos = 1;
    Carpeta directorioActual;
    Carpeta directorioAnterior;
    String rutaActual;
    String three;
    String rutas;
    String tab=" \t &nbsp;&nbsp;";

    public MiFileSystem() {
        root = new Carpeta("root", 0);
        directorioActual = root;
        rutaActual = "/";
    }

    public void crearCarpeta(String nombre) {
        Carpeta carpeta = directorioActual.getDirectorio(nombre);
        if (carpeta == null) {
            directorioActual.crearCarpeta(nombre, idCarpetas);
            idCarpetas++;
        }else{
            System.out.println("Ya existe una carpeta con ese nombre ");
        };
    }

    public String getRutaActual() {
        //System.out.println(rutaActual);
        return rutaActual;
    }

    public void cambiarDirectorio(String nombre) {
        Carpeta carpeta = directorioActual.getDirectorio(nombre);
        if (carpeta != null) {
            directorioAnterior = directorioActual;
            directorioActual = carpeta;
            rutaActual +=  carpeta.getNombre()+"/" ;
        }
        else{
            System.out.println("No se encontró la carpeta");
        }
    }

    public String getEspacioLibre() {
        String espacioLibre = discoV.espacioLibre();
        return espacioLibre;
    }

    public Carpeta getDirectorioActual() {
        return directorioActual;
    }

    //public void irAAnterior() {
      //  String[] directorios = rutaActual.split("/");
       // rutaActual = "/";
       // for (int i=0; i<directorios.length-1 ; i++) {
         //   if (!directorios[i].equals("")) {
           //     rutaActual += directorios[i]+"/";
            //}
        //}
        //directorioActual = directorioAnterior;
    //}

    public void irAAnterior(){
        String[] directorios = getRutaActual().split("/");
        //String destino = directorios[directorios.length-1] ;

        String ruta = "/";
        for (int i = 1; i < directorios.length; i++) {
            if (i<directorios.length-1){
                ruta+= directorios[i]+"/";
            }
        }
        System.out.println(ruta);
        irRuta(ruta);
        System.out.println(mostrarEstadoFileSystem());

    };

    public void irRuta(String nombre){
        String[] directorios = nombre.split("/");
        String ruta = "";
        for (int i = 1; i < directorios.length; i++) {
            ruta+= directorios[i]+"/";
        }
        directorios = ruta.split("/");
        irARoot();
        for (String carp:directorios){
            cambiarDirectorio(carp);
        }
        //System.out.println(mostrarEstadoFileSystem());
    };

    public void irARoot() {
        rutaActual = "/";
        directorioActual = root;
    }

    public void crearArchivo(String nombre, String contenido){
        Archivo archivo = directorioActual.getArchivo(nombre);
        if (archivo == null) {
            List<Integer> sectores = guardarEnDisco(contenido);
            if (!sectores.isEmpty()){
                byte[] bytes = contenido.getBytes(StandardCharsets.UTF_8);
                int tamano = bytes.length;
                directorioActual.crearArchivo(nombre, idArchivos, sectores, tamano);
                idArchivos++;
            } else {
                System.out.println("No hay espacio suficiente");
            }
        }else{
            System.out.println("Ya existe un archivo con ese nombre ");
        };
    }

    public String verInfoArchivo(String nombre) {
        return directorioActual.verInfoArchivo(nombre);
    }

    public String verContArchivo(String nombre) {
        List<Integer> sectores = directorioActual.getSectoresArchivo(nombre);
        return getContenido(sectores);
    }

    public void crearDisco(int tamano, int sectores) {
        discoV = new DiscoV();
        discoV.inicializar(tamano, sectores);
    }

    public List<Integer> guardarEnDisco(String contenido) {
        List<Integer> sectores = discoV.guardar(contenido);
        return sectores;
    }

    public void eliminarEnDisco(List<Integer> sectores){
        discoV.eliminar(sectores);
    }

    public String getContenido(List<Integer> sectores) {
        return discoV.obtener(sectores);
    }

    public void actualizarEnDisco(List<Integer> sectores, String contenido) {
        List<Integer> newsectores = discoV.actualizar(sectores, contenido);
        if (!newsectores.isEmpty()) {
            // reemplazar los sectores al archivo
        }
    }

    public String mostrarEstadoFileSystem() {
        String estado = rutaActual+"\n";
        estado += directorioActual.getEstado();
        return estado;
    }

    //-----------------------------------

    //Crea archivo de Real al Virtual
    public String obtenerArchivoReal(String nombre, String ruta){
        String textoFinal="";
        try{
            BufferedReader bf= new BufferedReader(new FileReader(ruta+""+nombre));

            String texto="";
            String bfReader;
            while((bfReader= bf.readLine())!=null){
                texto=texto+bfReader;
            }
            textoFinal=texto;
        }catch(IOException ex){ex.printStackTrace();}
        return textoFinal;
    }

    //Crea el archivo Real
    public void fileReal(String nombre,String texto, String ruta){

        ruta=ruta+"";

        File directorio = new File(ruta);
        File file= new File(directorio,nombre);
        FileWriter w;
        PrintWriter wr;
        if(!file.exists()){
            try{
                file.createNewFile();
                System.out.println(file.getName()+" Se creo en la ruta real con éxito");
                w= new FileWriter(file);
                BufferedWriter bw = new BufferedWriter(w);
                wr = new PrintWriter(bw);

                wr.write(texto);
                wr.close();
                bw.close();
            }catch(IOException ex){ex.printStackTrace();}
        }

    }

    public String copy(int instruccion, String nombre, String ruta){
        String texto="";
        if (instruccion==1){
            //Copiar archivo de real a virtual
            texto=obtenerArchivoReal( nombre,ruta);


        }else if(instruccion==2){

            //Copiar archivo de virtual a real

            String info="";
            info=verContArchivo(nombre);
            fileReal(nombre, info,ruta);
            System.out.println("Se creo la copía del archivo: " + nombre+" En la ruta: "+ruta);

        }else if(instruccion==3){

            //Copiar archivo de virtual a  virtual

            String info="";
            info=verContArchivo(nombre);
            //Detectar ruta
            cambiarDirectorio(ruta);
            crearArchivo(nombre,info);
            System.out.println("Se creo la copía del archivo: " + nombre+" En la ruta: "+ruta);

        }else{
            System.out.println("Digite una opción valida");
        }
        return texto;
    }

    //Buscar elemento en la ruta

    public String findDatoCarpeta(String nombre){

        if(directorioActual.findCarpeta(nombre)==true){
            return rutaActual;
        }
        return "";
    }

    //Imprimir Three


    public String verDisco() {
        return discoV.mostrar();
    }

    public void actualizarArchivo(String nombre, String contenido){
        eliminarEnDisco(directorioActual.getSectoresArchivo(nombre)); //limpia los sectores del contenido anterior
        List<Integer> sectores = guardarEnDisco(contenido); //guarda el nuevo contenido en el disco
        if (!sectores.isEmpty()){
            byte[] bytes = contenido.getBytes(StandardCharsets.UTF_8);
            int tamano = bytes.length;
            directorioActual.udpateFile(nombre,sectores,tamano);
        } else {
            System.out.println("No hay espacio suficiente");
        }
    }

    public void eliminarArchivo(String nombre){
        eliminarEnDisco( directorioActual.getSectoresArchivo(nombre));
        directorioActual.eliminarArchivo(nombre);
    };

     public Integer obtenerSizeCarpeta(String nombre){
         return directorioActual.getSizeCarpetas();
    };

     public List<Carpeta> obtenerCarpetas(String nombre){
         return directorioActual.getCarpetas();
     };

     public void eliminarArchivosCarpeta(){
         List<Archivo> archivos = directorioActual.getArchivos();
         String name  = null;
         Integer result=0;
         for (Archivo archivo : archivos) {
             name = archivo.getNombre();
             eliminarArchivo(name);
             result+=1;
         }
     }


     public String getRutas(String nombre){
         Carpeta temp=directorioActual;
         rutas="<html>";
         find(root,nombre);
         directorioActual=temp;
         listaRutas(nombre);
         rutas+="</html>";
         return rutas;

     }
     public String getThree(){
         Carpeta temp=directorioActual;
         three="<html>";
         revisar(root);
         directorioActual=temp;
         listaArchivos();
         three+="</html>";
         return three;
     }

    public void listaRutas(String nombre){

        for(Archivo ar:directorioActual.getArchivos()){
            if(ar.getNombre()==nombre){

                rutas+=rutaActual+"\n<br>";
            }else if(ar.getNombre().endsWith(nombre)){
                rutas+=rutaActual+"\n<br>";
            }

        }
    }
     public Integer find(Carpeta carpeta, String nombre){
         List<Carpeta> lista = carpeta.getCarpetas();
         for (Carpeta car:lista){
             if (car.getSizeCarpetas()>0){
                 rutaActual+=car.getNombre()+"/";
                 directorioActual=car;
                 listaRutas(nombre);
                 System.out.println(find(car,nombre));
             }
             else{
                 rutaActual+=car.getNombre()+"/";
                 directorioActual=car;
                 listaRutas(nombre);
             }
             irAAnterior();
             irAAnterior();
         }
         return 1;
     }

     public void listaArchivos(){
         for(Archivo ar:directorioActual.getArchivos()){
             three+="\t &nbsp;&nbsp;&nbsp;&nbsp;";
             three+="\t &nbsp;&nbsp;&nbsp;&nbsp;";
             three+=ar.getNombre()+"\n<br>";
         }
     }

    public Integer revisar(Carpeta carpeta) {
        three+=tab;
        List<Carpeta> lista = carpeta.getCarpetas();
        for (Carpeta car:lista){
            three+=tab;
            if (car.getSizeCarpetas()>0){
                directorioActual=car;
                three+=	">> \t "+car.getNombre()+"\n<br>";
                listaArchivos();
                three+=tab;
                System.out.println(revisar(car));
            }
            else{
                directorioActual=car;
                three+=tab;
                three+=">> \t "+car.getNombre()+"\n<br>";
                three+=tab;
                listaArchivos();

            }
        }
        return 1;
    };
     public Integer llamarVersion2(String nombre){
         Carpeta temp = directorioActual;
         Carpeta directorio =directorioActual.getDirectorio(nombre);
         eliminarCarpetaVersion2(directorio);
         directorioActual=directorio;
         eliminarArchivosCarpeta();
         directorioActual=temp;
         directorioActual.eliminarCarpetaVacia(directorio.nombre);
         return 1;
     }

     public Integer eliminarCarpetaVersion2(Carpeta carpeta){
         List<Carpeta> lista = carpeta.getCarpetas();
         for (Carpeta car:lista){
             if (car.getSizeCarpetas()>0){
                 directorioActual=car;
                 eliminarArchivosCarpeta();
                 System.out.println(eliminarCarpetaVersion2(car));
             }
             else{
                 directorioActual=car;
                 eliminarArchivosCarpeta();
             }
         }
         return 1;
     };

    public String rutaDestinoEsArchivo(String nombre){
        String[] directorios = nombre.split("/");
        String destino = directorios[directorios.length-1] ;

        String ruta = "/";
        for (int i = 1; i < directorios.length; i++) {
            if (i<directorios.length-1){
                ruta+= directorios[i]+"/";
            }
        }
        irRuta(ruta);
        for (Carpeta carp:directorioActual.getCarpetas()){
            if (carp.getNombre().equals(destino)){
                return "esCarpeta";
            }
        }

        for (Archivo arch:directorioActual.getArchivos()){
            if (arch.getNombre().equals(destino)){
                return "esArchivo";
            }
        }
        return "false";
    }

    public void renameFile(String rutaOrigen , String rutaDestino){
        String[] directorioOrigen = rutaOrigen.split("/");
        String archivoActual = directorioOrigen[directorioOrigen.length-1] ;
        String[] directorioDestino= rutaDestino.split("/");
        String archivoFuturo = directorioDestino[directorioOrigen.length-1] ;

        String ruta = "/";
        for (int i = 1; i < directorioOrigen.length; i++) {
            if (i<directorioOrigen.length-1){
                ruta+= directorioOrigen[i]+"/";
            }
        }
        irRuta(ruta);
        directorioActual.renameFile(archivoActual , archivoFuturo);
        System.out.println(mostrarEstadoFileSystem());
    };

    public void renameFolder(String rutaOrigen , String rutaDestino){
        String[] directorioOrigen = rutaOrigen.split("/");
        String folderActual = directorioOrigen[directorioOrigen.length-1] ;
        String[] directorioDestino= rutaDestino.split("/");
        String folderFuturo = directorioDestino[directorioOrigen.length-1] ;

        String ruta = "/";
        for (int i = 1; i < directorioOrigen.length; i++) {
            if (i<directorioOrigen.length-1){
                ruta+= directorioOrigen[i]+"/";
            }
        }
        irRuta(ruta);
        directorioActual.renameFolder(folderActual , folderFuturo);
        System.out.println(mostrarEstadoFileSystem());
    };

    public void changeFileFolder(String directorioFile , String directorioFolder){
        String[] directorioOrigen = directorioFile.split("/");
        String file = directorioOrigen[directorioOrigen.length-1] ;

        String ruta = "/";
        for (int i = 1; i < directorioOrigen.length; i++) {
            if (i<directorioOrigen.length-1){
                ruta+= directorioOrigen[i]+"/";
            }
        }
        irRuta(ruta);
        String contenido =verContArchivo(file);
        eliminarArchivo(file);
        System.out.println(mostrarEstadoFileSystem());
        irRuta(directorioFolder);
        crearArchivo(file,contenido);
        System.out.println(mostrarEstadoFileSystem());
    };

    public List<String> getNameFiles(String ruta){
        List<String> listNames = new ArrayList<String>();
        irRuta(ruta);
        List<Archivo> listArchivos= directorioActual.getArchivos();
        for (Archivo file:listArchivos){
            listNames.add(file.getNombre());
        }
        return listNames;
    };

    public List<String> getContFiles(String ruta){
        List<String> listCont= new ArrayList<String>();
        irRuta(ruta);
        List<Archivo> listArchivos= directorioActual.getArchivos();
        for (Archivo file:listArchivos){
            listCont.add(getContenido(file.getSectores()));
        }
        return listCont;
    };

    public void crearArchivos(String rutaOrigen , String rutaDestino){
        List<String> names = getNameFiles(rutaOrigen);
        List<String> conts = getContFiles(rutaOrigen);
        irRuta(rutaOrigen);
        eliminarArchivosCarpeta();
        irRuta(rutaDestino);
        Integer i;
        for (i = 0; i < names.size(); i++) {
            crearArchivo(names.get(i), conts.get(i));
        }
        System.out.println(mostrarEstadoFileSystem());
    };

    public Integer remove(Carpeta carpeta , String rutaOrigen , String directorioDestino){
        irRuta(rutaOrigen);
        List<Carpeta> lista = carpeta.getCarpetas();
        crearArchivos(rutaOrigen , directorioDestino);
        for (Carpeta car:lista){
            irRuta(directorioDestino);
            crearCarpeta(car.nombre);
            if (car.getSizeCarpetas()>0){
                irRuta(rutaOrigen+"/"+car.nombre);
                crearArchivos(rutaOrigen+"/"+car.nombre , directorioDestino+"/"+car.nombre);
                System.out.println(remove(car , rutaOrigen+"/"+car.nombre , directorioDestino+"/"+car.nombre));
            }
            else{
                crearArchivos(rutaOrigen+"/"+car.nombre , directorioDestino+"/"+car.nombre);
            }
        }
        return 1;
    };

    public Integer changeFolderFolder(String rutaOrigen , String directorioDestino){
        String[] directorioOrigen = rutaOrigen.split("/");
        String folderActual = directorioOrigen[directorioOrigen.length-1] ;
        String ruta = "/";
        for (int i = 1; i < directorioOrigen.length; i++) {
            if (i<directorioOrigen.length-1){
                ruta+= directorioOrigen[i]+"/";
            }
        }
        irRuta(rutaOrigen);
        Carpeta temp = directorioActual;
        irRuta(directorioDestino);

        crearCarpeta(folderActual);
        directorioDestino +="/"+folderActual;
        remove(temp , rutaOrigen , directorioDestino);
        irRuta(ruta);
        directorioActual.eliminarCarpetaVacia(folderActual);

        return 1;

    };

    public Integer moverArchivo(String param1, String directorio){
        String[] directorio1 = param1.split("/");
        String[] directorio2 = directorio.split("/");
        String ruta1 = "/";
        for (int i = 1; i < directorio1.length; i++) {
            if (i<directorio1.length-1){
                ruta1+= directorio1[i]+"/";
            }
        }
        String ruta2 = "/";
        for (int i = 1; i < directorio2.length; i++) {
            if (i<directorio2.length-1){
                ruta2+= directorio2[i]+"/";
            }
        }
        System.out.println("Ruta1: " + ruta1 + " Ruta2: " + ruta2);
        if (rutaDestinoEsArchivo(param1).equals("esCarpeta") && rutaDestinoEsArchivo(directorio).equals("esCarpeta")){
            System.out.println("Mover la carpeta a otra carpeta");
            changeFolderFolder(param1 , directorio);
        }
        else if (rutaDestinoEsArchivo(param1).equals("esArchivo") && rutaDestinoEsArchivo(directorio).equals("esCarpeta")){
            System.out.println("Mover el archivo a la carpeta");
            changeFileFolder(param1 , directorio);
        }
        else if (ruta1.equals(ruta2)){
            System.out.println("Renombrar ...");
            if (rutaDestinoEsArchivo(param1).equals("esCarpeta") && rutaDestinoEsArchivo(directorio).equals("false")){
                System.out.println("Carpeta");
                renameFolder(param1 , directorio);
            }
            else if (rutaDestinoEsArchivo(param1).equals("esArchivo") && rutaDestinoEsArchivo(directorio).equals("false")){
                System.out.println("Archivo");
                renameFile(param1,directorio);
            }
        }
        else{
            System.out.println("Direcciones inválidas");
        }
        return 1;
    };

}
